#include <iostream>
/*fusion de deux monotonies dans les fichiers 
nomfic1.txt et nomfic2.txt dans le fichier nomficSortie.txt*/

#include <fstream>
#include <string>
using namespace std;

void fusion ( const string & nomfic1, const string & nomfic2, string & nomficSortie ) {
    /*les variables */
    double val1, val2;
    bool succeslect1 = true , succeslect2= true;
        ifstream fic1 ( nomfic1.c_str() );
        if(!fic1.is_open() ) {
            cout << "Erreur ouverture fichier " << nomfic1 << endl;
            exit(EXIT_FAILURE);
    }
    ifstream fic2 ( nomfic2.c_str() );
    if(!fic2.is_open() ) {
        cout << "Erreur ouverture fichier " << nomfic2 << endl;
        exit(EXIT_FAILURE);
    }
    ofstream ficSortie ( nomficSortie.c_str() );
    if(!ficSortie.is_open() ) {
        cout << "Erreur ouverture fichier " << nomficSortie << endl;
        exit(EXIT_FAILURE);
    }
    fic1>>val1;
    fic2>>val2;
    while ( succeslect1 && succeslect2 ) {
        if ( val1 < val2 ) {
            ficSortie << val1 << endl;
            fic1 >> val1;
            if(fic1.eof() ) {
                succeslect1 = false;
            }
        } else {
            ficSortie << val2 << endl;
            fic2 >> val2;
            if(fic2.eof() ) {
                succeslect2 = false;
            }
        }
        
    }
    if(!succeslect1 ) {
        do {
            ficSortie << val2 << endl;
            fic2 >> val2;
        } while ( !fic2.eof() );
    } else {
        do {
            ficSortie << val1 << endl;
            fic1 >> val1;
        } while ( !fic1.eof() );

    }
    fic1.close();
    fic2.close();
    ficSortie.close();
    

}

int main() {
    string nomfic1, nomfic2, nomficSortie;
    cout << "Donner le nom du premier fichier: ";
    cin >> nomfic1;
    cout << "Donner le nom du deuxieme fichier: ";
    cin >> nomfic2;
    cout << "Donner le nom du fichier de sortie: ";
    cin >> nomficSortie;
    fusion(nomfic1, nomfic2, nomficSortie);
    return 0;
   
}