#include <iostream>
#include <fstream>

int main() {
    // Ouvrir ou créer un fichier en mode écriture
    std::ofstream fichier("monfichier.txt");
    if (fichier.is_open()) {
        fichier << "Bonjour, monde ! ici c'est ok " << std::endl; // Écrire dans le fichier
        fichier.close(); // Fermer le fichier
        std::cout << "Écriture réussie !" << std::endl;
    } else {
        std::cout << "Erreur d'ouverture du fichier en écriture." << std::endl;
    }
    return 0;
}
