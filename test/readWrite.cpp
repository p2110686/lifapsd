#include <iostream>
#include <fstream>

int main() {
    std::fstream fichier("monfichier.txt", std::ios::in | std::ios::out | std::ios::app); // Lecture et écriture
    if (fichier.is_open()) {
        fichier << "Nouvelle ligne ajoutée." << std::endl; // Écrire dans le fichier
        fichier.seekg(0); // Revenir au début du fichier pour la lecture
        std::string ligne;
        while (getline(fichier, ligne)) {
            std::cout << ligne << std::endl; // Lire le contenu
        }
        fichier.close(); // Fermer le fichier
    } else {
        std::cout << "Erreur d'ouverture du fichier." << std::endl;
    }
    return 0;
}
