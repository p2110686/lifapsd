#include <iostream>
#include <fstream>

int main() {
    std::ofstream fichierEcriture("exemple.txt");
    fichierEcriture << "Ceci est un fichier texte." << std::endl;
    fichierEcriture.close();

    std::ifstream fichierLecture("exemple.txt");
    std::string ligne;
    while (getline(fichierLecture, ligne)) {
        std::cout << ligne << std::endl;
    }
    fichierLecture.close();

    return 0;
}
