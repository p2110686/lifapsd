#include <iostream>
#include "TableauDynamique.h"
#include "ElementTD.h"
using namespace std;
// test 

void TableauDynamique::Concatener(const TableauDynamique & t) {
    for (unsigned int i = 0; i < t.taille_utilisee; i++) {
        ajouterElement(t.ad[i]);
    }
}

int main( ) {
    TableauDynamique tableau1 , tableau2;
    /*tirer entre 1 et 10 (nombre choisi aleatoirement ) entiers tires aleatoirement dans l'intervalle [-50,50] et les ajouter dans tableau 1*/
    for (unsigned int i = 0; i < 10; i++) {
        tableau1.ajouterElement(ElementTD(rand() % 101 - 50));

    }
    
    /*tirer entre 1 et 10 (nombre choisi aleatoirement ) entiers tires aleatoirement dans l'intervalle [-50,50] et les ajouter dans tableau2*/
    for (unsigned int i = 0; i < 10; i++) {
        tableau2.ajouterElement(ElementTD(rand() % 101 - 50));
    }
    /*afficher les deux tableaux*/
    tableau1.afficher();
    tableau2.afficher();
    /*concatener les deux tableaux*/
    tableau1.Concatener(tableau2);
    /*afficher le tableau resultant*/
    tableau1.afficher();
    return 0;
}