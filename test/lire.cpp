#include <iostream>
#include <fstream>
#include <string>

int main() {
    std::ifstream fichier("monfichier.txt"); // Ouvrir le fichier en mode lecture
    if (fichier.is_open()) {
        std::string ligne;
        while (getline(fichier, ligne)) { // Lire ligne par ligne
            std::cout << ligne << std::endl;
        }
        fichier.close(); // Fermer le fichier
    } else {
        std::cout << "Erreur d'ouverture du fichier en lecture." << std::endl;
    }
    return 0;
}
