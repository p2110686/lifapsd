/*arbre apres insertion
[5,2,[2,2[1,3],[3,2]],[8,3[6,4],[9,1]]]
*/
#include <iostream>
#include "ElementA.h"

struct Noeud {
    ElementA info ;
    Noeud fg, fd ;
    unsigned int occurence ;
};
void insererElement(ElementA e){
    if (e < info){
        if (fg == NULL){
            fg = new Noeud(e);
        } else {
            fg->insererElement(e);
        }
    } else if (e > info){
        if (fd == NULL){
            fd = new Noeud(e);
        } else {
            fd->insererElement(e);
        }
    } else {
        occurence++;
    }
}