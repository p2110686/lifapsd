#include <iostream>
#include <cmath>

/* TP1 */
/* Exerceice 3 */

struct Vecteur3D {
    float x,y,z;
};
/*une fonction Vecteur3DGetNorme qui retourne la norme d’un vecteur*/
float Vecteur3DGetNorme(Vecteur3D  & v){
    return sqrt ( v.x*v.x+v.y*v.y+v.z*v.z);
}

/*une procédure Vecteur3DNormaliser qui normalise le vecteur passé en paramètre*/
void Vecteur3DNormaliser(Vecteur3D & v){
    float norme = Vecteur3DGetNorme(v); 
    v.x = v.x/norme;
    v.y = v.y/norme;
    v.z = v.z/norme;
   }

/* Vecteur3DEstNormalise */

bool Vecteur3DEstNormalise ( Vecteur3D & v ){
    float norme = Vecteur3DGetNorme(v); 
    if(norme == 1 ){return true;}
    else return false;
}
/* une fonction Vecteur3DAdd qui retourne le vecteur somme de deux vecteurs passés en paramètre */
Vecteur3D Vecteur3DAdd (Vecteur3D & v1,Vecteur3D & v2){
    Vecteur3D  v; 
    v.x = v1.x+v2.x;
    v.y=v1.y+v2.y;
    v.z=v1.z+v2.z;
    return v;
}

void Vecteur3DAfficher(Vecteur3D  v){
    std::cout<< "vecteur = ( "<< v.x << ","<< v.y<< "," << v.z<<")"<<std::endl;
}

/* Exercice 4  */

void Vecteur3DRemplirTabVecteurs(Vecteur3D t[], int taille ){
   // t = new Vecteur3D [taille] ; 
  
    for (int i = 0; i < taille; i++) {
        Vecteur3D v;
        v.x = ((rand()% 200 ) - 100)/10.0;
        v.y = ((rand()% 200 ) - 100)/10.0;
        v.z = ((rand()% 200 ) - 100)/10.0;
        t[i]= v;
    }

}

int main(){
     srand((unsigned int)time(NULL));  
    

    Vecteur3D vecteur1 = {5,2,1};
    Vecteur3D vecteur2 = {0,3,2}; 
    
    
    /*
    float norme1 = Vecteur3DGetNorme(vecteur1);
    Vecteur3DNormaliser(vecteur1);
    norme1 = Vecteur3DGetNorme(vecteur1);
    std::cout << Vecteur3DEstNormalise(vecteur1)<<std::endl;
    std::cout<< "la valeur de norme 1 est : "<< norme1 << std::endl;
    Vecteur3DAdd(vecteur1,vecteur2);
    
    */
    

    /*main prof */



    std::cout << "vecteur1 non normalise: ";
    Vecteur3DAfficher(vecteur1);
    std::cout << std::endl;

    std::cout << "vecteur2 non normalise: ";
    Vecteur3DAfficher(vecteur2);
    std::cout << std::endl;

    std::cout << "somme: ";
    Vecteur3DAfficher(Vecteur3DAdd(vecteur1,vecteur2));
    std::cout << std::endl;

    Vecteur3DNormaliser(vecteur1);
    Vecteur3DNormaliser(vecteur2);
    
    std::cout << "vecteur1 normalise: ";
    Vecteur3DAfficher(vecteur1);
    std::cout << std::endl;
    
    std::cout << "vecteur2 normalise: ";
    Vecteur3DAfficher(vecteur2);
    std::cout << std::endl;
    
    std::cout << "somme: ";
    Vecteur3D somme = Vecteur3DAdd(vecteur1,vecteur2);
    Vecteur3DAfficher(somme);
    if (Vecteur3DEstNormalise(somme)) std::cout << " est normalise" << std::endl;
    else std::cout << " n'est pas normalise" << std::endl;


/*Exo 4  */
    int taille= 5; 
    Vecteur3D t [taille];
    Vecteur3DRemplirTabVecteurs(t,taille);
    for(int i=0;i<taille; i++ ){
        Vecteur3DAfficher(t[i]);
        std::cout<<std::endl;
    }
    return 0;
}