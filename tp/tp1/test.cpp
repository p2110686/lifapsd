#include <stdlib.h> // pour srand() et rand()
#include <time.h> // pour time()
void Vecteur3DRemplirTabVecteur (Vecteur3D  tab [] , int taille  ){
    for(int i=0; i<taille; i++){
        tab[i].x = ((rand()% 200 ) - 100)/10.0;
        tab[i].y = ((rand()% 200 ) - 100)/10.0;
        tab[i].z = ((rand()% 200 ) - 100)/10.0;
    }
}

void Vecteur3DRemplirTabVecteurs(Vecteur3D t[], int taille ){
   // t = new Vecteur3D [taille] ; 
  
    for (int i = 0; i < taille; i++) {
        Vecteur3D v;
        v.x = ((rand()% 200 ) - 100)/10.0;
        v.y = ((rand()% 200 ) - 100)/10.0;
        v.z = ((rand()% 200 ) - 100)/10.0;
        t[i]= v;
    }

}

void Vecteur3DRemplirTabVecteurs(Vecteur3D t[], int taille ){
   // t = new Vecteur3D [taille] ; 
  
    for (int i = 0; i < taille; i++) {
        Vecteur3D v;
        v.x = (((float)rand()/(float)RAND_MAX))*20.0-10.0;
        v.y = (((float)rand()/(float)RAND_MAX)) *20.0-10.0;
        v.z = (((float)rand()/(float)RAND_MAX))*20.0-10.0;
        t[i]= v;
    }

}


int main() {
    int aleatoire;
    int min = 1, max = 31;
    int nombreDeValeurs = max – min + 1 ;
    /* Initialisation du générateur, à ne faire qu’une fois dans le programme */
    srand((unsigned int)time(NULL));
    /* Tirage de 100 entiers aléatoires compris entre min et max inclus */
    for (int i = 0; i < 100; i++) aleatoire = (rand() % nombreDeValeurs) + min;
    return 0;
}

/*what is different srand and rand */