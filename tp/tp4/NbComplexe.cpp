#include <cmath>
#include <fstream>
#include <iostream>

//
// Created by adam on 14/10/24.
//
class NbComplexe {
public:
    float re,im;

    NbComplexe () : re(0), im(0) { }
    NbComplexe (float a, float b) : re(a), im(b) { }
    NbComplexe (const NbComplexe& c) {re = c.re; im = c.im;}
    ~NbComplexe () { }


    float module() const {
        return sqrt(re*re+im*im);
    }
    void saisir() {
        std::cout<<std::endl<<"Saisir la partie reelle : ";
        std::cin>>re;
        std::cout<<"Saisir la partie imaginaire : ";
        std::cin>>im;
    }

    void afficher() const {
        std::cout << re << " ";
        if (im >= 0.0) std::cout << "+";
        std::cout << im << " i";
        std::cout << std::endl;
    }
/*
    //multiplie le nombre complexe par un autre nombre complexe passé en paramètre
    void multiplier (const NbComplexe& c){
        float saveRe = re;
        re = re*c.re - im*c.im;
        im = im*c.re + saveRe*c.im;
    }


    bool estPlusPetit (const NbComplexe & c) const {
        return module() < c.module();
    }
*/
    /************************/
    // TP4
    // Exercice 1
    // Préambule : surchage d’opérateurs
    /* ( = ) affectation
     * ( < ) strictement
     * ( * ) multiplication
     * ( << ) ecriture
     * ( >> ) lecteure
     */
    NbComplexe& operator= (const NbComplexe& c) {
        re = c.re;
        im = c.im;
        return *this;
    }

    bool operator< (const NbComplexe& c) const {
        return module() < c.module();
    }
    void operator* (const NbComplexe& c) {
        float saveRe = re;
        re = re*c.re - im*c.im;
        im = im*c.re + saveRe*c.im;
    }
    std::ostream& operator<< (std::ostream& flux) {
        flux << re << " ";
        if (im >= 0.0) flux << "+";
        flux << im << " i";
        return flux;
    }
    std::istream& operator>> (std::istream& flux) {
        flux >> re >> im;
        return flux;
    }

};

// Exercice 5
void trierParSelection (NbComplexe * tab, int taille) {
    NbComplexe minComplexe;
    for (int i=0; i<taille-1; i++) {
        int indmin = i;
        for (int j=i+1; j<taille; j++) {
            if (tab[j].module() < tab[indmin].module()) indmin = j;

        }
        minComplexe = tab[indmin];
        tab[indmin] = tab[i];
        tab[i] = minComplexe;
    }
}

// Exercice 6

void trierParInsertion (NbComplexe * tab, int taille) {
    NbComplexe complexeAPlacer;
    for (int i=1; i<taille; i++) {
        complexeAPlacer = tab[i];
        int j = i - 1;
        while ( j >= 0 && complexeAPlacer.module() < tab[j].module()) {
            tab[j+1] = tab[j];
            j--;
        }
        tab[j+1]=complexeAPlacer;
    }
}


//lecture txt

void lireTabNbComplexeDepuisFichier (NbComplexe * tab, int & taille, const char * nom_fichier) {
    std::ifstream fichier("random.txt");
    if (!fichier) {
        std::cerr << "Erreur d'ouverture du fichier" << std::endl;
        return;
    }
    fichier >> taille;
    tab = new NbComplexe[taille];
    for (int i=0; i<taille; i++) {
        fichier >> tab[i].re >> tab[i].im;
    }
    fichier.close();
}


int main () {

    NbComplexe unComplexe;
    unComplexe.afficher();
    unComplexe.saisir();
    unComplexe.afficher();
// Exercice 2
    NbComplexe complexe1 (unComplexe);
    complexe1.afficher();
    NbComplexe * complexe2 = new NbComplexe(5,2);
    complexe2->afficher();
    complexe1 = *complexe2;

    complexe2->afficher();
    delete complexe2;

    // Exercice 4
    int taille;
    std::cout << "Donner le nombre de complexes: ";
    std::cin >> taille;
    if (taille <= 0) return 0;
    srand((unsigned int) time(NULL));
    NbComplexe * tab = new NbComplexe[taille];

    for (int i=0; i<taille; i++) {
        tab[i].re = ((rand()%201)/10.0)-10.0;
        tab[i].im = ((rand()%201)/10.0)-10.0;
        std::cout << "tab[" << i << "]= ";
        std::cout << "(mod= " << tab[i].module() << ") ";
        tab[i].afficher();
    }



    // Exercice 5

    trierParSelection(tab,taille);
    for (int i=0; i<taille; i++) {
        std::cout << "tab trie[" << i << "]= ";
        std::cout << "(mod= " << tab[i].module() << ") ";
        tab[i].afficher();
    }



    //Exercice 6
    for (int i=0; i<taille; i++) {
        tab[i].re = ((rand()%201)/10.0)-10.0;
        tab[i].im = ((rand()%201)/10.0)-10.0;
        std::cout << "tab[" << i << "]= ";
        std::cout << "(mod= " << tab[i].module() << ") ";
        tab[i].afficher();
    }
    trierParInsertion(tab,taille);


    for (int i=0; i<taille; i++) {
        std::cout << "tab trie[" << i << "]= ";
        std::cout << "(mod= " << tab[i].module() << ") ";
        tab[i].afficher();
    }
    delete [] tab;
    // test d'ouverture de fichier




    return 0;
}
