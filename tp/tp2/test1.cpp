#include <iostream>
using namespace std;

/*calcul d'un coefficient binomial à l'aide du triangle de Pascal */
/*  *pre condition : p <= n , p >= 0
    *post condition :
    *resultat : c est la comb p dans n 
*/


int comb(int n, int p ){
    int tmp1,  tmp2;
    cout <<"Calcul du nb de combinaisons de "<<p<<" elts parmi "<<n<<endl;
    if ((p==0) || (n==p))
        return 1;
    tmp1 = comb(n-1, p-1); /* premier appel récursif */
    tmp2 = comb(n-1, p); /* second appel récursif */

    return tmp1 + tmp2;
}

/*
int comb(int n, int p, int &resultat){
    int tmp1,  tmp2;
    cout <<"Calcul du nb de combinaisons de "<<p<<" elts parmi "<<n<<endl;
    if ((p==0) || (n==p))
        return 1;
    // int & resultat1 ;
    tmp1 = comb(n-1, p-1, resultat); 
    tmp2 = comb(n-1, p, resultat); 
    resultat = tmp1 + tmp2;
    return resultat;
}
*/



void comb1(int n, int p, int &resultat){
    int tmp1,  tmp2;
    cout <<"Calcul du nb de combinaisons de "<<p<<" elts parmi "<<n<<endl;
    if ((p==0) || (n==p))
        resultat = 1;

    // int & resultat1 ;
    tmp1 = comb1(n-1, p-1, resultat); 
    tmp2 = comb1(n-1, p, resultat); 
    resultat = tmp1 + tmp2;
    
}


/* void comb (int n, int p, int *monPointer){
    int tmp1,  tmp2;
    cout <<"Calcul du nb de combinaisons de "<<p<<" elts parmi "<<n<<endl;
    if ((p==0) || (n==p))
        return 1;
    tmp1 = comb(n-1, p-1, *monPointer); 
    tmp2 = comb(n-1, p, *monPointer);
    int *monPointer1 = new int;
    *monPointer1 = comb(n, p, *monPointer);
    cout << "comb1  vaut " <<*monPointer1<< endl;
    delete monPointer1;

    

} 
*/

int main() {
    int c;
    c = comb(4, 3);
    cout << "c vaut " << c << endl;

    int resultat;
    int c1 ;
    comb1(4, 3,  resultat);
    cout << "comb1  vaut " <<c1<< endl;
    cout << "resultat vaut " << resultat<< endl;




    /*test 4.3 */
    int *monPointer;
    //comb(4, 3, monPointer);
    cout << "comb2  vaut " <<*monPointer<< endl;
    delete monPointer;



    return 0;
}