
#include <iostream>



int main() {
    /* test tableau  */
    int taille;
    std::cout << "entrez taille souhaiter :" << std::endl;
    std::cin>> taille; 
    /*allocation du tableau*/
    int *tab = new int[taille];

    /* remplissage du tableau */
    for(int i=0;i<taille; i++ ){
        std::cout << "entrez une valeur pour la position " << i << " :" << std::endl;
        std::cin>> tab[i];   
    }

    /* affichage du tableau */
    std::cout << "tableau : [ ";
    for(int i=0;i<taille-1; i++ ){
        std::cout << tab[i] << " , ";   
    }
    std::cout << tab[taille-1];  
    std::cout << "]"<<std::endl;


   
   
    delete [] tab;  
   
    return 0;
}