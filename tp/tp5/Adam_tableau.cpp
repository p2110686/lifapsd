#include <iostream>
#include "ElementTD.h"
#include "TableauDynamique.h"

/*procedure insererTabDyn */
void TableauDynamique::insererTabDyn(TableauDynamique &t, float indince) {
    /*inserer par indice les elements*/
    for(int i = 0 ; i < t.taille_utilisee ; i++){
        this->insererElement(t.valeurIemeElement(i),indince);
        indince++;
    }

}
/* implementation insereerTabDynTest */
void TableauDynamique::insererTabDynTest(TableauDynamique *t, float indice) {

}
int main() {
    std::cout << "Hello, World!" << std::endl;
    /* declaration sur le tas tableau1 et tableau2 avec new */
    TableauDynamique tableau1 = new TableauDynamique();
    TableauDynamique tableau2 = new TableauDynamique();

    srand(time(NULL));

    for(int i = 0 ; i < 10 ; i++){
        tableau1.ajouterElement(rand()%100-50);
        tableau2.ajouterElement(rand()%100-50);
    }

    tableau1.afficher();
    tableau2.afficher();
    std::cout << "afficher indice tab1 alea "<<std::endl;
    /**/
    tableau1.insererTabDyn(tableau2, 5 );
    /*avec la boucle pour inserer tous les valeurs */

    
    for(int i = 0 ; i < tableau1.taille_utilisee ; i++){
        std::cout << tableau1.valeurIemeElement(i) << " ";
    }
    std::cout << std::endl;
    tableau2.afficher();
    /*afficher le tableau2 après insertion*/
    
    return 0;
}