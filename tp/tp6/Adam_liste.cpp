#include <iostream>


#include <vector>
#include "ElementL.h"
#include "Liste.h"

/*on converti string en liste ou on stock les vraies valleurs du caractere char c */
void Liste::convertirStringEnListe (std::string & texte , unsigned int taille){
    for (unsigned int i = 0; i < taille; i++){
        ajouterEnQueue((ElementL) texte[i]);
    }
}
/* la fonction qui renvoie vrai ou faux d'un nombre premier */
bool Liste::estPremier(int n){
    if (n < 2){
        return false;
    }
    for (int i = 2; i < n; i++){
        if (n % i == 0){
            return false;
        }
    }
    return true;
}
/*test my fonction by Adam Aysoy*/

/*la procedure qui fait les deux listes premier et pas premiers*/
void Liste::premiersPasPremiers(Liste & premiers, Liste & pasPremiers){
    Cellule * courant = prem;
    while (courant != nullptr){
        if (estPremier(courant->info)){
            premiers.ajouterEnQueue(courant->info);
        }else{
            pasPremiers.ajouterEnQueue(courant->info);
        }
        courant = courant->suivant;
    }
}
int main(){
    std::cout << "begin main" << std::endl;


    Liste l1;
    l1.ajouterEnQueue(1);
    l1.ajouterEnQueue(2);
    l1.ajouterEnQueue(7);
    l1.afficherGaucheDroite();
    std::string texte ;
    //srand(time(NULL));
    for(int i = 0; i <20 ; i++){
        //srand(time(NULL));
        texte += (char) (rand() % 26 + 97);
    }
    std::cout << texte << std::endl;
    l1.convertirStringEnListe(texte, 20);
    l1.afficherGaucheDroite();

    /*autre annales tp */
    Liste l2;
    for(int i = -2; i < 31; i++){
        l2.ajouterEnQueue(i);
    }
    std::cout << "la liste l2 est : " << std::endl;
    l2.afficherGaucheDroite();
    Liste prem, pasPrem;
    l2.premiersPasPremiers(prem, pasPrem);
    prem.afficherGaucheDroite();
    pasPrem.afficherGaucheDroite();
    /*fin annales tp */
   
    return 0;
}