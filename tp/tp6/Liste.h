#ifndef _LISTE
#define _LISTE

#include "ElementL.h"
#include <string>




struct Cellule {
    ElementL info;
    Cellule * suivant;
    Cellule * precedent;
};

class Liste {
public:
    /* Données membres */
    /* =============== */
    Cellule * prem;
    Cellule * last;

    /* Fonctions membres */
    /* ================= */
    Liste ();
    /* Postcondition : la liste est initialement vide */

    ~Liste ();
    /* Postcondition : la mémoire allouée sur le tas est libérée */

    Liste& operator = (const Liste & l);
    /* Postcondition : la liste correspond à une copie de l ( mais les 2 listes sont totalement indépendantes l'une de l'autre) */

    void vider ();
    /* Postcondition : la liste ne contient plus aucune cellule */

    bool estVide () const;
    /* Résultat : vrai si liste vide, faux sinon */

    unsigned int nbElements () const;
    /* Résultat : nombre d'éléments contenus dans la liste */

    ElementL iemeElement (unsigned int indice) const;
    /* Precondition : la liste n'est pas vide, et 0 <= indice < nombre d'éléments
       Résultat : valeur de l'élément à l'indice en paramètre, sachant que les éléments sont numérotes à partir de 0 */

    void modifierIemeElement (unsigned int indice, ElementL e);
    /* Precondition : la liste n'est pas vide, et 0 <= indice < nombre d'éléments
       Postcondition : e remplace l'élément à l'indice en paramètre, sachant que les éléments sont numérotes à partir de 0 */

    void afficherGaucheDroite () const;
    /* Postcondition : affichage de tous les éléments, en commençant par le premier (espacement entre les éléments) */

    void afficherDroiteGauche () const;
    /* Postcondition : affichage de tous les éléments, en commençant par le dernier (espacement entre les éléments) */

    void ajouterEnTete (ElementL e);
    /* Postcondition : e est ajouté en tête de liste */

    void ajouterEnQueue (ElementL e);
    /* Postcondition : e est ajouté en fin de liste */

    void supprimerTete ();
    /* Precondition : la liste n'est pas vide
       Postcondition : la liste perd son premier élément */

    int rechercherElement (ElementL e) const;
    /* Résultat : position de la première occurrence de e dans la liste (en partant de la gauche),
                  -1 si e n'est pas dans la liste. Les cellules sont numérotées à partir de 0, donc une valeur de retour égale à 0
                  signifie que la première occurrence de e se trouve dans la première cellule de la liste. */

    void insererElement (ElementL e, unsigned int indice);
    /* Preconditions : 0 <= indice <= nombre d'éléments
       Postconditions : une copie indépendante de e est insérée de sorte qu'elle occupe la position d'indice en paramètre */

    void trier ();
    /* Postcondition : la liste est triée dans l'ordre croissant des valeurs des éléments */

/************** test tpn *********** */

    void convertirStringEnListe (std::string & texte , unsigned int taille);
    /*precondition : taille est le nombre de caracteres dans texte
     * postcondition : la liste de l'instance contient les codes ASCII des caracteres de texte
     parametres en mode donnée : texte, taille 
    */
   bool estPremier(int n);
   /*
   * Resultat : vrai si n est premier et faux sinon
   * parametres en mode donnée : n
   */
  void premiersPasPremiers(Liste & premiers, Liste & pasPremiers);
   
};

#endif
