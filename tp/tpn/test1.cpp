#include <iostream>
struct Date
{
    // int year;
    unsigned int month ;
    unsigned int day;
};
void saisir(Date &d)
{
    std::cout << "Enter the month: ";
    std::cin >> d.month;
    std::cout << "Enter the day: ";
    std::cin >> d.day;
}
void afficher(const Date &d)
{
    std::cout << "The month is: " << d.month << std::endl;
    std::cout << "The day is: " << d.day << std::endl;
}
int lelandemain(const Date &d)
{
    Date demain;
    demain.day = d.day + 1;
    demain.month = d.month;
    if (demain.day > 31)
    {
        demain.day = 1;
        demain.month++;
    }
    return demain.day;
}
int main()
{
    Date d;
    saisir(d);
    afficher(d);
    std::cout << "The day after is: " << lelandemain(d) << std::endl;
    return 0;
}