#include <iostream>
#include <cmath>

struct TableauReels
{
    float *tab;
    unsigned int taille;
};
// demande le nombre d'elements detableau et remplir aleatoirement entre -5.0 et 5.0
void saisi(TableauReels &t)
{
    std::cout << "Enter the size of the array: ";
    std::cin >> t.taille;
    t.tab = new float[t.taille];
    for (int i = 0; i < t.taille; i++)
    {
        t.tab[i] = -5.0 + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (5.0 - (-5.0))));
    }
}
// afficher le tableau
void affiche(const TableauReels &t)
{
    for (int i = 0; i < t.taille; i++)
    {
        std::cout << "The element at index " << i << " is: " << t.tab[i] << std::endl;
    }
}
// cumule deux tableaux
TableauReels cumul(const TableauReels &t1, const TableauReels &t2)
{
    TableauReels t;
    t.taille = t1.taille;
    t.tab = new float[t.taille];
    for (int i = 0; i < t.taille; i++)
    {
        t.tab[i] = t1.tab[i] + t2.tab[i];
    }
    return t;
}
int main()
{
    TableauReels t1, t2, t3;
    saisi(t1);
    affiche(t1);
    saisi(t2);
    affiche(t2);
    t3 = cumul(t1, t2);
    affiche(t3);
    delete[] t1.tab;
    delete[] t2.tab;
    delete[] t3.tab;
    return 0;
}
