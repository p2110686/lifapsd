#include <iostream>
class Duree
{
public:
    unsigned minutes;
    unsigned int secondes;
    Duree(){
        std::cout << "saisir la  minute: ";
        std::cin >> minutes;
        // verif la seconde entre 0 et 59 
        std::cout << "saisir la seconde entre 0 et 59 : ";
        std::cin >> secondes;
        while(secondes < 0 || secondes > 59)
        {
            std::cout << "La seconde doit etre entre 0 et 59" << std::endl;
            std::cout << "saisir la seconde entre 0 et 59 : ";
            std::cin >> secondes;
            //on peut faire limite de test sur les secondes. mais non preciser dans le sujet

        }
        
        
    }
    void afficher()
    {
        std::cout << "La duree est: " << minutes <<"'" <<secondes << "''" << std::endl;
    }
    void ajouter (const Duree &d)
    {
        minutes += d.minutes;
        secondes += d.secondes;
        if (secondes >= 60)
        {
            minutes++;
            secondes -= 60;
        }
    }
};
int main()
{
    Duree d1, d2;
    d1.afficher();
    d2.afficher();
    // test ajout de 
    d1.ajouter(d2);
    d1.afficher();
    return 0;
}